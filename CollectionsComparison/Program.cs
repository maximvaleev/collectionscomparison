﻿/* Домашнее задание
Сравнение коллекций

Цель:
Сделать сравнение по скорости работы List, ArrayList и LinkedList.

Описание/Пошаговая инструкция выполнения домашнего задания:
1    Создать коллекции List, ArrayList и LinkedList.
2    С помощью цикла for добавить в каждую 1 000 000 элементов (1,2,3,...).
3    С помощью Stopwatch.Start() и Stopwatch.Stop() замерить длительность заполнения каждой коллекции и вывести значения на экран.
4    Найти 496753-ий элемент, замерить длительность этого поиска и вывести на экран.
5    Вывести на экран каждый элемент коллекции, который без остатка делится на 777. Вывести длительность этой операции для каждой коллекции.
*    Укажите сколько времени вам понадобилось, чтобы выполнить это задание.

Критерии оценки:
Пункты 1-3 - 6 баллов, Пункт 4 - 2 балла, Пункт 5 - 2 балла
Для зачёта домашнего задания достаточно 6 баллов.
 */

using System.Collections;
using System.Diagnostics;

namespace CollectionsComparison
{
    internal class Program
    {
        const int ElementCount = 1000000;
        const int ValueToSearch = 496753;
        const int ValueToMod = 777;
        static Stopwatch _watch = new();

        static List<int> _list = new(ElementCount);
        static ArrayList _arrayList = new(ElementCount);
        static LinkedList<int> _linkedList = new();


        static void Main(string[] args)
        {
            Console.WriteLine("Сравнение эффективности коллекций.\n");

            Fill();

            Search();

            Modulus();

            AddressByIndex();
        }


        private static void Fill()
        {
            Console.WriteLine("*** Тест скорости заполнения коллекций ***");
            
            _watch.Restart();
            for (int i = 1; i <= ElementCount; i++)
                _list.Add(i);
            _watch.Stop();
            Console.WriteLine($"Заполнение List<int> заняло:       {_watch.ElapsedTicks} тиков");

            _watch.Restart();
            for (int i = 1; i <= ElementCount; i++)
                _arrayList.Add(i);
            _watch.Stop();
            Console.WriteLine($"Заполнение ArrayList заняло:       {_watch.ElapsedTicks} тиков");

            _watch.Restart();
            for (int i = 1; i <= ElementCount; i++)
                _linkedList.AddLast(i);
            _watch.Stop();
            Console.WriteLine($"Заполнение LinkedList<int> заняло: {_watch.ElapsedTicks} тиков\n");
        }


        private static void Search()
        {
            Console.WriteLine("*** Тест скорости поиска элемента в коллекциях ***");

            _watch.Restart();
            _ = _list.Find(x => x == ValueToSearch);
            _watch.Stop();
            Console.WriteLine($"Поиск в List<int> занял:       {_watch.ElapsedTicks} тиков");

            _watch.Restart();
            _ = _arrayList[_arrayList.IndexOf(ValueToSearch)];
            _watch.Stop();
            Console.WriteLine($"Поиск в ArrayList занял:       {_watch.ElapsedTicks} тиков");

            _watch.Restart();
            _ = _linkedList.Find(ValueToSearch)?.Value;
            _watch.Stop();
            Console.WriteLine($"Поиск в LinkedList<int> занял: {_watch.ElapsedTicks} тиков\n");
        }


        private static void Modulus()
        {
            long listTicks, arrayListTicks, linkedListTicks;
            
            Console.WriteLine("*** Тест скорости обхода коллекций ***");

            Console.Write($"\nВывод всех элементов, делящихся на {ValueToMod} в List<int>: ");
            _watch.Restart();
            foreach (int i in _list)
                if (i % ValueToMod == 0)
                    Console.Write(i + " ");
            _watch.Stop();
            listTicks = _watch.ElapsedTicks;

            Console.Write($"\n\nВывод всех элементов, делящихся на {ValueToMod} в ArrayList: ");
            _watch.Restart();
            foreach (int i in _arrayList)
                if (i % ValueToMod == 0) 
                    Console.Write(i + " ");
            _watch.Stop();
            arrayListTicks = _watch.ElapsedTicks;

            Console.Write($"\n\nВывод всех элементов, делящихся на {ValueToMod} в LinkedList<int>: ");
            _watch.Restart();
            foreach (int i in _linkedList)
                if (i % ValueToMod == 0) 
                    Console.Write(i + " ");
            _watch.Stop();
            linkedListTicks = _watch.ElapsedTicks;

            Console.WriteLine($"\n\nОперация в List<int> заняла:       {listTicks} тиков");
            Console.WriteLine($"Операция в ArrayList заняла:       {arrayListTicks} тиков");
            Console.WriteLine($"Операция в LinkedList<int> заняла: {linkedListTicks} тиков\n");
        }


        private static void AddressByIndex()
        {
            Console.WriteLine("*** Тест скорости обращения к элементу по индексу ***");
            
            _watch.Restart();
            _ = _list[ValueToSearch];
            _watch.Stop();
            Console.WriteLine($"Обращение по индексу в List<int> заняло:       {_watch.ElapsedTicks} тиков");

            _watch.Restart();
            _ = _arrayList[ValueToSearch];
            _watch.Stop();
            Console.WriteLine($"Обращение по индексу в ArrayList заняло:       {_watch.ElapsedTicks} тиков");

            _watch.Restart();
            LinkedListNode<int> currentNode = _linkedList.First;
            for (int i = 0; i <= ValueToSearch; i++)
            {
                currentNode = currentNode.Next;
            }
            _ = currentNode.Value;
            _watch.Stop();
            Console.WriteLine($"Обращение по индексу в LinkedList<int> заняло: {_watch.ElapsedTicks} тиков");
        }
    }
}